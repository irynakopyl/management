﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.DAL.Models
{
    public class Task
    {
        //[JsonProperty("id")]
        public int Id { get; set; }
        //[JsonProperty("name")]
        public string Name { get; set; }
        //[JsonProperty("description")]
        public string Description { get; set; }
        //[JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        //[JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }
       // [JsonProperty("state")]
        public TaskState TaskState { get; set; }

        //[JsonProperty("projectId")]
        public int ProjectId { get; set; }

        //[JsonProperty("performerId")]
        public int PerformerId { get; set; }

        public User Performer { get; set; }
    }
}
