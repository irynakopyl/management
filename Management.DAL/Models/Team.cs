﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.DAL.Models
{
    public class Team
    {
        //[JsonProperty("id")]
        public int Id { get; set; }
       // [JsonProperty("name")]
        public string Name { get; set; }
        //[JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

    }
}
