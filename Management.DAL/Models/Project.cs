﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.DAL.Models
{
    public class Project
    {
        public Project()
        {
            Tasks = new List<Task>();
        }
        //[JsonProperty("id")]
        public int Id { get; set; }
        //[JsonProperty("name")]
        public string Name { get; set; }
        //[JsonProperty("description")]
        public string Description { get; set; }
        //[JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        //[JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        //[JsonProperty("authorId")]
        public int AuthorId { get; set; }
        //[JsonProperty("teamId")]
        public int TeamId { get; set; }

        public List<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
