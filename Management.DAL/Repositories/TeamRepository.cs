﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;

namespace Management.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly IEnumerable<Team> _dataSet;
        public TeamRepository(IEnumerable<Team> data)
        {
            _dataSet = data;
        }

        public IEnumerable<Team> GetAll()
        {
            return _dataSet;
        }

        public IEnumerable<Team> Get(Func<Team, bool> filter = null)
        {
            IEnumerable<Team> query = _dataSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.ToList();
        }

        public void Create(Team entity)
        {
            if (entity != null)
            {
                _dataSet.Append(entity);
            }
        }

        public void Update(Team entity)
        {
            if (entity != null)
            {
                Team upd = _dataSet.First(x => x.Id == entity.Id);
                if (upd != _dataSet.First(x => x.Id == entity.Id))
                {
                    _dataSet.ToList().Remove(upd);
                    _dataSet.Append(entity);
                }
            }
        }

        public IEnumerable<Team> Find(Func<Team, Boolean> predicate)
        {
            return _dataSet.Where(predicate).ToList();
        }

        public void Delete(Team entity)
        {
            if (entity != null)
                _dataSet.ToList().Remove(entity);
        }
    }
}
