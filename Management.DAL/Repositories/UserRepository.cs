﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;

namespace Management.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly IEnumerable<User> _dataSet;
        public UserRepository(IEnumerable<User> data)
        {
            _dataSet = data;
        }

        public IEnumerable<User> GetAll()
        {
            return _dataSet;
        }

        public IEnumerable<User> Get(Func<User, bool> filter = null)
        {
            IEnumerable<User> query = _dataSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.ToList();
        }

        public void Create(User entity)
        {
            _dataSet.Append(entity);
        }

        public void Update(User entity)
        {
            if (entity != null)
            {
                User upd = _dataSet.First(x => x.Id == entity.Id);
                if (upd != _dataSet.First(x => x.Id == entity.Id))
                {
                    _dataSet.ToList().Remove(upd);
                    _dataSet.Append(entity);
                }
            }
        }

        public IEnumerable<User> Find(Func<User, Boolean> predicate)
        {
            return _dataSet.Where(predicate).ToList();
        }

        public void Delete(User entity)
        {
            if (entity != null)
                _dataSet.ToList().Remove(entity);
        }
    }
}
