﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;

namespace Management.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly IEnumerable<Project> _dataSet;
        public ProjectRepository(IEnumerable<Project> data)
        {
            _dataSet = data;
        }

        public IEnumerable<Project> GetAll()
        {
            return _dataSet;
        }

        public IEnumerable<Project> Get(Func<Project, bool> filter = null)
        {
            IEnumerable<Project> query = _dataSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.ToList();
        }

        public void Create(Project entity)
        {
            _dataSet.Append(entity);
        }

        public void Update(Project entity)
        {
            if (entity != null)
            {
                Project upd = _dataSet.First(x => x.Id == entity.Id);
                if (upd != _dataSet.First(x => x.Id == entity.Id))
                {
                    _dataSet.ToList().Remove(upd);
                    _dataSet.Append(entity);
                }
            }
        }

        public IEnumerable<Project> Find(Func<Project, Boolean> predicate)
        {
            return _dataSet.Where(predicate).ToList();
        }

        public void Delete(Project entity)
        {

            if (entity != null)
                _dataSet.ToList().Remove(entity);
        }
    }
}
