﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Management.DAL.Interfaces;

namespace Management.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> _dataSet;
        public Repository(IEnumerable<TEntity> data)
        {
            _dataSet = data;
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dataSet;
        }

        public IEnumerable<TEntity> Get(Func<TEntity,bool> filter = null)
        {
            IEnumerable<TEntity> query = _dataSet;
            if(filter != null)
            {
                query = _dataSet.Where(filter);
            }
            return query.ToList();
        }

        public void Create(TEntity entity)
        {
            _dataSet.Append(entity);
        }

        public void Update(TEntity entity)
        {
           TEntity upd= _dataSet.First(x => x == entity); 
        }

        public IEnumerable<TEntity> Find(Func<TEntity, Boolean> predicate)
        {
            return _dataSet.Where(predicate).ToList();
        }

        public void Delete(TEntity entity)
        {
           
            if (entity != null)
                _dataSet.ToList().Remove(entity);
        }
    }
}
