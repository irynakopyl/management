﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;

namespace Management.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly IEnumerable<Task> _dataSet;
        public TaskRepository(IEnumerable<Task> data)
        {
            _dataSet = data;
        }

        public IEnumerable<Task> GetAll()
        {
            return _dataSet;
        }

        public IEnumerable<Task> Get(Func<Task, bool> filter = null)
        {
            IEnumerable<Task> query = _dataSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            return query.ToList();
        }

        public void Create(Task entity)
        {
            _dataSet.Append(entity);
        }

        public void Update(Task entity)
        {
            if (entity != null)
            {
                Task upd = _dataSet.First(x => x.Id == entity.Id);
                if (upd != _dataSet.First(x => x.Id == entity.Id))
                {
                    _dataSet.ToList().Remove(upd);
                    _dataSet.Append(entity);
                }
            }
        }
        public IEnumerable<Task> Find(Func<Task, Boolean> predicate)
        {
            return _dataSet.Where(predicate).ToList();
        }

        public void Delete(Task entity)
        {

            if (entity != null)
                _dataSet.ToList().Remove(entity);
        }
    }
}
