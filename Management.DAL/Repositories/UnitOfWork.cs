﻿using Management.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Models;
using System.Xml.Linq;

namespace Management.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool disposed = false;
        public UnitOfWork(IRepository<User> users, IRepository<Team> teams, IRepository<Project> projects, IRepository<Task> tasks)
        {
            UsersRepo = users;
            TeamsRepo = teams;
            ProjectsRepo = projects;
            TasksRepo = tasks;
        }
        public IRepository<User> UsersRepo { get; set; }
        public IRepository<Team> TeamsRepo { get; set; }
        public IRepository<Project> ProjectsRepo { get; set; }
        public IRepository<Task> TasksRepo { get; set; }


        public void Save()
        {
            //if we`d use DB, we`ll save its state there
        }
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //if we`d use DB, we`ll dispose it there
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
