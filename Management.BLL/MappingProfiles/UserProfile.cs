﻿using Management.BLL.DTO;
using Management.DAL.Models;
using AutoMapper;

namespace Management.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}
