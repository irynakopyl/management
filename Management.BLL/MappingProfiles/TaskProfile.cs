﻿using Management.BLL.DTO;
using Management.DAL.Models;
using AutoMapper;

namespace Management.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<TaskDTO, Task>();
        }
    }
}
