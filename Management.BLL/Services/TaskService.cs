﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class TaskService : BaseService
    {
        public TaskService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {}
        public TaskDTO CreateTask(TaskDTO newTask)
        {
            var taskEntity = _mapper.Map<Task>(newTask);
            _context.TasksRepo.Create(taskEntity);
            var createdTask = _context.TasksRepo.Find(x => x.Id == taskEntity.Id).First();
            return _mapper.Map<TaskDTO>(createdTask);
        }
        public ICollection<TaskDTO> GetTasks()
        {
            var tasks = _context.TasksRepo.GetAll().ToList();
            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }
        public TaskDTO GetTaskById(int id)
        {
            if (!_context.TasksRepo.Get(x => x.Id == id).Any()) throw new ArgumentException("No tasks");
            Task task = _context.TasksRepo.Get(x => x.Id == id).First();
            if (task == null)
            {
                throw new ArgumentException("Invalid id");
            }
            return _mapper.Map<TaskDTO>(task);
        }
        public TaskDTO UpdateTask(TaskDTO task)
        {
            var t = _context.TasksRepo.Find(x => x.Id == task.Id).First();
            if (t == null) throw new ArgumentException("Invalid task");
            var result = _mapper.Map<Task>(task);
            _context.TasksRepo.Delete(t);
            _context.TasksRepo.Create(result);
            return _mapper.Map<TaskDTO>(_context.TasksRepo.Find(x => x.Id == result.Id).First());
        }
        public void DeleteTask(int id)
        {
            var taska = _context.TasksRepo.Find(x => x.Id == id).First();
            if (taska == null) throw new ArgumentException("Invalid id");
            //search the project where task was
            var proj = _context.ProjectsRepo.Get(x => x.Id == taska.ProjectId).First();
            _context.TasksRepo.Delete(taska);
            if (proj != null)
            {
                _context.ProjectsRepo.Get(x => x.Id == proj.Id).First().Tasks.Remove(taska);
            }//if the project already exists, remove task
        }
        public ICollection<TaskDTO> GetAllTasksWithNameLessThan45Chars(int userId)
        {
            return _mapper.Map<List<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(
                p => p.Tasks.Where(
                    x => x.PerformerId == userId && x.Name.Length < 45)).
                    ToList();
        }

    }
}
