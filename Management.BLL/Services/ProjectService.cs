﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.BLL.Services.Abstract;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class ProjectService: BaseService
    {
        public ProjectService(IUnitOfWork context, IMapper mapper ): base(context, mapper)
        {
            
        }
        public ProjectDTO CreateProject(ProjectDTO newTeam)
        {
            var projectEntity = _mapper.Map<Project>(newTeam);
            _context.ProjectsRepo.Create(projectEntity);
            var createdUsr = _context.ProjectsRepo.Find(x => x.Id == projectEntity.Id).First();
            return _mapper.Map<ProjectDTO>(createdUsr);
        }
        public ICollection<ProjectDTO> GetProjects()
        {
            var projects = _context.ProjectsRepo.GetAll().ToList();
            return _mapper.Map<ICollection<ProjectDTO>>(projects);
        }
        public ProjectDTO GetProjectById(int id)
        {
            if (!_context.ProjectsRepo.Get(x => x.Id == id).Any()) throw new ArgumentException("No project");
            Project proj = _context.ProjectsRepo.Get(x => x.Id == id).First();
            if (proj == null)
            {
                throw new ArgumentException("Invalid id");
            }
            return _mapper.Map<ProjectDTO>(proj);
        }
        public ProjectDTO UpdateProject(ProjectDTO task)
        {
            var p = _context.ProjectsRepo.Find(x => x.Id == task.Id).First();
            if (p == null) throw new ArgumentException("Invalid task");
            var result = _mapper.Map<Project>(task);
            _context.ProjectsRepo.Delete(p);
            _context.ProjectsRepo.Create(result);
            return _mapper.Map<ProjectDTO>(_context.ProjectsRepo.Find(x => x.Id == result.Id).First());
        }
        public void DeleteProject(int id)
        {
            var proj = _context.ProjectsRepo.Find(x => x.Id == id).First();
            if (proj == null) throw new ArgumentException("Invalid id");
            _context.ProjectsRepo.Delete(proj);
        }
        public List<ProjectInfoDTO> GetAllProjectsAndItsTasksInfo()
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Select(t => t.Performer)).Distinct().GroupBy(x => x.TeamId)
               .Select(x => new TeamListDTO
               {
                   Id = x.Key,
                   Members = x.ToList()
               }
               ).Join(
               _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()),
                team => team.Id,
                user => user.TeamId,
               (t, u) =>
               new ProjectInfoDTO
               {
                   Proj = u,
                   LongestTask = u.Tasks.OrderByDescending(p => p.Description).First(),
                   ShortestTask = u.Tasks.OrderBy(p => p.Name).First(),
                   UserAmount = t.Members.Count()
               }).ToList();
        }
    }
}
