﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.Services.Abstract;
using Management.BLL.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {

        }
        public UserDTO CreateUser(UserDTO newUser)
        {
            var userEntity = _mapper.Map<User>(newUser);
            _context.UsersRepo.Create(userEntity);
            var createdUser = _context.TasksRepo.Find(x => x.Id == userEntity.Id).First();
            return _mapper.Map<UserDTO>(createdUser);
        }
        public ICollection<UserDTO> GetUsers()
        {
            var teams = _context.UsersRepo.GetAll().ToList();
            return _mapper.Map<ICollection<UserDTO>>(teams);
        }
        public UserDTO GetUserById(int id)
        {
            if (!_context.TeamsRepo.Get(x => x.Id == id).Any()) throw new ArgumentException("No users");
            User user = _context.UsersRepo.Get(x => x.Id == id).First();
            if (user == null)
            {
                throw new ArgumentException("Invalid id");
            }
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO UpdateUser(UserDTO task)
        {
            var u = _context.UsersRepo.Find(x => x.Id == task.Id).First();
            if (u == null) throw new ArgumentException("Invalid task");
            var result = _mapper.Map<User>(task);
            _context.UsersRepo.Delete(u);
            _context.UsersRepo.Create(result);
            return _mapper.Map<UserDTO>(_context.UsersRepo.Find(x => x.Id == result.Id).First());
        }
        public void DeleteUser(int id)
        {
            var usr = _context.UsersRepo.Find(x => x.Id == id).First();
            if (usr == null) throw new ArgumentException("Invalid id");
            _context.UsersRepo.Delete(usr);
        }
        public IDictionary<string, int> GetTaskAmountInProjects(int userId)
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll())
                .Select(
                p => (p, p.Tasks.Where(
                    x => x.PerformerId == userId).ToList().Count)).ToDictionary(p => p.p.Name, p => p.Item2);
        }
        public ICollection<ReadyTaskInCurrentYearDTO> GetAllTasksFinishedInCurrentYear(int userId)
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(
                p => p.Tasks.Where(x => x.TaskState == TaskStateDTO.Finished && x.FinishedAt.Year == 2020 && x.PerformerId == userId))
                .Select(t =>
               new ReadyTaskInCurrentYearDTO
               {
                   Id = t.Id,
                   Name = t.Name
               }).ToList();
        }
        public ICollection<TasksListForUserDTO> GetAllSortedUsersWithSortedTasks()
        {
            IEnumerable<TaskDTO> tasks = _mapper.Map<IEnumerable<TaskDTO>>(_context.TasksRepo.GetAll());
            IEnumerable<UserDTO> uzzers = _mapper.Map<IEnumerable<UserDTO>>(_context.UsersRepo.GetAll());
            return uzzers.GroupJoin(tasks,
                 user => user.Id,
                 task => task.PerformerId,
                (t, u) =>  new TasksListForUserDTO
                 {
                     Performer = t,
                     Tasks = u.OrderByDescending(t => t.Name.Length).ToList()
                 }
                ).OrderBy(t => t.Performer.FirstName).ToList();
        }
        public UserInfoDTO GetTasksAndProjectsInfo(int userId)
        {
            UserInfoDTO user = new UserInfoDTO() { };
            user.LastProject = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First();//* Останній проект користувача(за датою створення)
            user.TaskAmountUnderLastProject = user.LastProject.Tasks.Count;//* Загальна кількість тасків під останнім проектом
            user.User = user.LastProject.Author;
            user.LongestTask = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Where(p => p.PerformerId == userId)) //* Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
               .Distinct()
               .OrderByDescending(t => t.FinishedAt - t.CreatedAt).First();

            user.NotReadyTasks = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Where(
                p => (p.PerformerId == userId && (p.TaskState == TaskStateDTO.Canceled || p.TaskState == TaskStateDTO.Started)))).Distinct().Count();//* Загальна кількість незавершених або скасованих тасків для користувача
            return user;

        }
    }
}
