﻿using Management.BLL.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class TeamService : BaseService
    {
        public TeamService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
        }
        public TeamDTO CreateTeam(TeamDTO newTeam)
        {
            var teamEntity = _mapper.Map<Team>(newTeam);
            _context.TeamsRepo.Create(teamEntity);
            var createdTeam = _context.TasksRepo.Find(x => x.Id == teamEntity.Id).First();
            return _mapper.Map<TeamDTO>(createdTeam);
        }
        public ICollection<TeamDTO> GetTeams()
        {
            var teams = _context.TeamsRepo.GetAll().ToList();
            return _mapper.Map<ICollection<TeamDTO>>(teams);
        }
        public TeamDTO GetTeamById(int id)
        {
            if (!_context.TeamsRepo.Get(x => x.Id == id).Any()) throw new ArgumentException("No teams");
            Team team = _context.TeamsRepo.Get(x => x.Id == id).First();
            if (team == null)
            {
                throw new ArgumentException("Invalid id");
            }
            return _mapper.Map<TeamDTO>(team);
        }
        public TeamDTO UpdateTeam(TeamDTO task)
        {
            var t = _context.TeamsRepo.Find(x => x.Id == task.Id).First();
            if (t == null) throw new ArgumentException("Invalid task");
            var result = _mapper.Map<Team>(task);
            _context.TeamsRepo.Delete(t);
            _context.TeamsRepo.Create(result);
            return _mapper.Map<TeamDTO>(_context.TeamsRepo.Find(x => x.Id == result.Id).First());
        }
        public void DeleteTeam(int id)
        {
            var team = _context.TeamsRepo.Find(x => x.Id == id).First();
            if (team == null) throw new ArgumentException("Invalid id");
            _context.TeamsRepo.Delete(team);
        }
        public List<TeamListDTO> GetTeamsWithUsersOlderThan10()
        {
            var temas = _mapper.Map<IEnumerable<TeamDTO>>(_context.TeamsRepo.GetAll());
            var uzzers = _mapper.Map<List<UserDTO>>(_context.UsersRepo.GetAll());
            return temas
                .GroupJoin(
                uzzers,
                 team => team.Id,
                 user => user.TeamId,
                (t, u) =>(
                new TeamListDTO
                {
                    Id = t.Id,
                    TeamName = t.Name,
                    Members = u.Where(x=> (2020 - x.Birthday.Year) > 10).OrderByDescending(u => u.RegisteredAt).ToList()
                 })).Where(team=>team.Members.Any()).ToList();
        }
}
}
