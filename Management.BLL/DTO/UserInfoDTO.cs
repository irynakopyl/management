﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{
    public class UserInfoDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int TaskAmountUnderLastProject { get; set; }
        public int NotReadyTasks { get; set; }
        public TaskDTO LongestTask { get; set; }
    }
}
