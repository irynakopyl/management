﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{
    public class TeamListDTO
    {
        public int? Id { get; set; }
        public string TeamName { get; set; }
        public List<UserDTO> Members { get; set; }
    }
}
