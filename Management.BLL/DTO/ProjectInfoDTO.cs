﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{
    public class ProjectInfoDTO
    {
        public ProjectDTO Proj { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int UserAmount { get; set; }
    }
}
