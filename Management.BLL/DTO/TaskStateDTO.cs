﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{

    public enum TaskStateDTO
    {
        Created = 0,
        Started = 1,
        Finished = 2,
        Canceled = 3
    }

}
