﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{
    public class ReadyTaskInCurrentYearDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
