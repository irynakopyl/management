﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.BLL.DTO
{
    public class TasksListForUserDTO
    {
        public UserDTO Performer { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
