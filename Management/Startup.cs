using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Management.DAL.Repositories;
using Management.BLL.Services;
using Management.BLL.MappingProfiles;
using Newtonsoft.Json;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using AutoMapper;
using System.IO;
using System.Reflection;

namespace Management
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<UserService>();
            services.AddScoped<UserService>();
            services.AddScoped<TaskService>();
            services.AddScoped<ProjectService>();
            services.AddScoped<TeamService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IRepository<User>, UserRepository>();
            services.AddScoped<IRepository<Project>, ProjectRepository>();
            services.AddScoped<IRepository<Team>, TeamRepository>();
            services.AddScoped<IRepository<DAL.Models.Task>, TaskRepository>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile<ProjectProfile>();
                mc.AddProfile<UserProfile>();
                mc.AddProfile<TaskProfile>();
                mc.AddProfile<TeamProfile>();

            });
            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton<IMapper>(mapper);

            var tasks = JsonConvert.DeserializeObject<List<DAL.Models.Task>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Tasks.txt")));
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Users.txt")));
            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Teams.txt")));
            var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Projects.txt")));
            //mapping entities
            //getting performer
            tasks = tasks.Join(
                users,
                t => t.PerformerId,
                x => x.Id,
                (task, performer) => new DAL.Models.Task
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    CreatedAt = task.CreatedAt,
                    FinishedAt = task.FinishedAt,
                    TaskState = task.TaskState,
                    ProjectId = task.ProjectId,
                    PerformerId = task.PerformerId,
                    Performer = performer
                }
                ).ToList();
            //getting Author
            projects = projects.Join(
                users,
                t => t.AuthorId,
                x => x.Id,
                (proj, author) => new Project
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    AuthorId = proj.AuthorId,
                    TeamId = proj.TeamId,
                    Team = proj.Team,
                    Tasks = proj.Tasks,
                    Author = author,
                }
                ).ToList().Join(
                    teams,
                    p => p.TeamId,
                    t => t.Id,
                    (proj, team) => new Project
                    {
                        Id = proj.Id,
                        Name = proj.Name,
                        Description = proj.Description,
                        CreatedAt = proj.CreatedAt,
                        Deadline = proj.Deadline,
                        AuthorId = proj.AuthorId,
                        TeamId = proj.TeamId,
                        Author = proj.Author,
                        Team = team,
                        Tasks = proj.Tasks
                    }
                ).ToList().GroupJoin(
                tasks,
                p => p.Id,
                t => t.ProjectId,
                (proj, task) => new Project()
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    AuthorId = proj.AuthorId,
                    TeamId = proj.TeamId,
                    Author = proj.Author,
                    Team = proj.Team,
                    Tasks = task.ToList()
                }
                ).ToList();

            services.AddSingleton<IEnumerable<DAL.Models.Task>>(tasks);
            services.AddSingleton<IEnumerable<Project>>(projects);
            services.AddSingleton<IEnumerable<Team>>(teams);
            services.AddSingleton<IEnumerable<User>>(users);


            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
