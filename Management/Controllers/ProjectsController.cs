﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.BLL.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {

        private readonly ProjectService _projectService;

        public ProjectsController(ProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpPost]
        public IActionResult Post([FromBody] ProjectDTO project)
        {
            ProjectDTO createdProject;
            if (ModelState.IsValid)
            {
                createdProject = _projectService.CreateProject(project);
                return Created("Post", createdProject);
            }
            return NotFound();
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> Get()
        {
            return Ok(_projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> GetById(int id)
        {
            return Ok(_projectService.GetProjectById(id));
        }

        [Route("GetAllProjectsAndItsTasksInfo")]
        public ActionResult<ICollection<ProjectInfoDTO>> GetAllProjectsAndItsTasksInfo()
        {
            return Ok(_projectService.GetAllProjectsAndItsTasksInfo());
        }

        [HttpPut]
        public ActionResult<ProjectDTO> Put([FromBody] ProjectDTO project)
        {
            _projectService.UpdateProject(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _projectService.DeleteProject(id);
            return NoContent();
        }
    }
}
