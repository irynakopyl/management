﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.BLL.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserService _userService;

        public UsersController(UserService userService)
        {
            _userService = userService;
        }
        [HttpPost]
        public IActionResult Post([FromBody] UserDTO user)
        {
            UserDTO createdUser;
            if (ModelState.IsValid)
            {
                createdUser = _userService.CreateUser(user);
                return Created("GetById", createdUser);
            }
            return NotFound();
           
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> Get()
        {
            return Ok(_userService.GetUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetById(int id)
        {
            return Ok( _userService.GetUserById(id));
        }

        [Route("GetTaskAmountInProjects/{id}")]
        public ActionResult<IDictionary<ProjectDTO,int>> GetTaskAmountInProjects(int id)
        {
            return Ok( _userService.GetTaskAmountInProjects(id));
        }

        [Route("GetAllTasksFinishedInCurrentYear/{id}")]
        public ActionResult<ICollection<ReadyTaskInCurrentYearDTO>> GetAllTasksFinishedInCurrentYear(int id)
        {
            return Ok(_userService.GetAllTasksFinishedInCurrentYear(id));
        }

        [Route("GetAllSortedUsersWithSortedTasks")]
        public ActionResult<ICollection<ReadyTaskInCurrentYearDTO>> GetAllSortedUsersWithSortedTasks()
        {
            return Ok(_userService.GetAllSortedUsersWithSortedTasks());
        }

        [Route("GetTasksAndProjectsInfo/{id}")]
        public ActionResult<UserInfoDTO> GetTasksAndProjectsInfo(int id)
        {
            return Ok(_userService.GetTasksAndProjectsInfo(id));
        }

        [HttpPut]
        public ActionResult<UserDTO> Put([FromBody] UserDTO user)
        {
            _userService.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.DeleteUser(id);
            return NoContent();
        }
    }
}
