﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.BLL.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {

        private readonly TaskService _taskService;

        public TasksController(TaskService teamService)
        {
            _taskService = teamService;
        }
        [HttpPost]
        public IActionResult Post([FromBody] TaskDTO task)
        {
            TaskDTO createdTask;
            if (ModelState.IsValid)
            {
                createdTask = _taskService.CreateTask(task);
                return Created("Post", createdTask);
            }
            return NotFound();
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> Get()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> GetById(int id)
        {
            return Ok(_taskService.GetTaskById(id));
        }
        [Route("GetAllTasksWithNameLessThan45Chars/{id}")]
        public ActionResult<ICollection<TeamListDTO>> GetAllTasksWithNameLessThan45Chars(int id)
        {
            return Ok(_taskService.GetAllTasksWithNameLessThan45Chars(id));
        }

        [HttpPut]
        public ActionResult<TaskDTO> Put([FromBody] TaskDTO task)
        {
            _taskService.UpdateTask(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _taskService.DeleteTask(id);
            return NoContent();
        }
    }
}
