﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Management.BLL.Services;
using Management.BLL.DTO;

namespace Management.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamsController(TeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpPost]
        public IActionResult Post([FromBody] TeamDTO team)
        {
            TeamDTO createdTeam;
            if (ModelState.IsValid)
            {
                createdTeam = _teamService.CreateTeam(team);
                return Created("Post", createdTeam);
            }
            return NotFound();
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> Get()
        {
            return Ok(_teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> GetById(int id)
        {
            return Ok(_teamService.GetTeamById(id));
        }

        [Route("GetTeamsWithUsersOlderThan10")]
        public ActionResult<ICollection<TeamListDTO>> GetTeamsWithUsersOlderThan10()
        {
            return Ok(_teamService.GetTeamsWithUsersOlderThan10());
        }

        [HttpPut]
        public ActionResult<TeamDTO> Put([FromBody] TeamDTO team)
        {
            _teamService.UpdateTeam(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _teamService.DeleteTeam(id);
            return NoContent();
        }
    }
}
