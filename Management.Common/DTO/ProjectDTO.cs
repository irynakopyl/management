﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Common.DTO
{
    public class ProjectDTO
    {
        public ProjectDTO()
        {
            Tasks = new List<TaskDTO>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public List<TaskDTO> Tasks { get; set; }
        public UserDTO Author { get; set; }
        public TeamDTO Team { get; set; }
    }
}
